import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './Pages/About/about.component';
import { BodyComponent } from './Pages/Body/body.component';
import { CareerComponent } from './Pages/Careers/career.component';
import { ContactComponent } from './Pages/Contacts/contact.component';
import { NewsComponent } from './Pages/News/news.component';
import { OurteamComponent } from './Pages/OurTeam/ourteam.component';
import { CargohandlingserviceComponent } from './Pages/Services/CargoHandlingService/cargohandlingservice.component';
import { CargosaleserviceComponent } from './Pages/Services/CargoSalesService/cargosaleservice.component';
import { GroundserviceComponent } from './Pages/Services/GroundServices/groundservice.component';
import { TickettravelserviceComponent } from './Pages/Services/TicketTravelService/tickettravelservice.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component';

const routes: Routes = [
  {
    path:'',component:LayoutComponent,children:[
    ]
  },
  {path:'Body',component:BodyComponent, pathMatch: 'full'},
  {path:'Groundservice',component:GroundserviceComponent, pathMatch: 'full'},
  {path:'Contact',component:ContactComponent, pathMatch: 'full'},
  {path:'CargoHandlingService',component:CargohandlingserviceComponent, pathMatch: 'full'},
  {path:'OurTeam',component:OurteamComponent, pathMatch: 'full'},
  {path:'Career',component:CareerComponent, pathMatch: 'full'},
  {path:'About',component:AboutComponent, pathMatch: 'full'},
  {path:'TicketTravel',component:TickettravelserviceComponent, pathMatch: 'full'},
  {path:'News',component:NewsComponent, pathMatch: 'full'},
  {path:'CargoSales',component:CargosaleserviceComponent, pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
