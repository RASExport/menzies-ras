import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './Pages/Home/home.component';
import { FooterComponent } from './Pages/Shared/Footer/footer.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component';
import { TopbarComponent } from './Pages/Shared/Topbar/topbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OwlModule } from 'ngx-owl-carousel';
import { BodyComponent } from './Pages/Body/body.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { GroundserviceComponent } from './Pages/Services/GroundServices/groundservice.component';
import { ContactComponent } from './Pages/Contacts/contact.component';
import { CargohandlingserviceComponent } from './Pages/Services/CargoHandlingService/cargohandlingservice.component';
import { OurteamComponent } from './Pages/OurTeam/ourteam.component';
import { CareerComponent } from './Pages/Careers/career.component';
import { AboutComponent } from './Pages/About/about.component';
import { TickettravelserviceComponent } from './Pages/Services/TicketTravelService/tickettravelservice.component';
import { NewsComponent } from './Pages/News/news.component';
import { CargosaleserviceComponent } from './Pages/Services/CargoSalesService/cargosaleservice.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    LayoutComponent,
    TopbarComponent,
    BodyComponent,
    GroundserviceComponent,
    ContactComponent,
    CargohandlingserviceComponent,
    OurteamComponent,
    CareerComponent,
    AboutComponent,
    TickettravelserviceComponent,
    NewsComponent,
    CargosaleserviceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    OwlModule,
    CarouselModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
